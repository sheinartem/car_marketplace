class CreateModels < ActiveRecord::Migration[6.1]
  def change
    create_table :models do |t|
      t.references :brand, foreign_key: true
      t.references :parent, index: true, foreign_key: { to_table: :models }
      
      t.string :name
      t.integer :cars_count
      t.string :description

      t.timestamps
    end
  end
end
