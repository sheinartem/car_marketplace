json.extract! model, :id, :name, :parent_id, :cars_count, :description, :created_at, :updated_at
json.url model_url(model, format: :json)
