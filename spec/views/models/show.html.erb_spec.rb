require 'rails_helper'

RSpec.describe "models/show", type: :view do
  before(:each) do
    assign(:model, Model.create!(
      name: "Name",
      parent_id: 2,
      cars_count: 3,
      description: "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Description/)
  end
end
