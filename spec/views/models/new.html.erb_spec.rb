require 'rails_helper'

RSpec.describe "models/new", type: :view do
  before(:each) do
    assign(:model, Model.new(
      name: "MyString",
      parent_id: 1,
      cars_count: 1,
      description: "MyString"
    ))
  end

  it "renders new model form" do
    render

    assert_select "form[action=?][method=?]", models_path, "post" do

      assert_select "input[name=?]", "model[name]"

      assert_select "input[name=?]", "model[parent_id]"

      assert_select "input[name=?]", "model[cars_count]"

      assert_select "input[name=?]", "model[description]"
    end
  end
end
