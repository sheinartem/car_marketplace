# == Schema Information
#
# Table name: models
#
#  id          :bigint           not null, primary key
#  brand_id    :integer
#  parent_id   :integer
#  name        :string
#  cars_count  :integer
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'rails_helper'

RSpec.describe Model, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:brand) }
  end
end

RSpec.describe Model, type: :model do
  describe 'associations' do
    it { is_expected.to have_many(:sub_models) }
  end
end

