require 'rails_helper'

RSpec.describe CarsController, type: :controller do
  describe "GET index" do
    let(:car1) { create(:car, name: 'Audi') }
    let(:car2) { create(:car, name: 'BMW') } 

    it 'assigns @cars' do
      get :index
      expect(assigns(:cars)).to eq([car1, car2])
    end
    
    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe 'GET show' do
    let(:car) { create(:car, name: 'Audi') }

    it 'assigns variables' do
      get :show, params: {id: car.id}
      expect(assigns(:car)).to eq(car)
    end

    it "has a 200 status code" do
      get :show, params: {id: car.id}
      expect(response.status).to eq(200)
    end
  end

  describe 'GET new' do
    let!(:car) { create(:car, name: 'Audi') }

    it 'assigns @car' do
      get :new
      expect(assigns(:car)).to be_a(Car)
    end
    
    it "has a 200 status code" do
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe 'POST create' do

    it 'creates car' do
      expect(Car.first).to be_nil
      post :create, params: {car: {name: 'Audi'} } 
      expect(Car.where(name: 'Audi').first).not_to be_nil
    end

    it "has a 302 status code" do
      post :create, params: {car: {name: 'Audi'} } 
      expect(response.status).to eq(302)
    end
  end

  describe 'PUT update' do  
    let!(:car) { create(:car, name: 'BMW') }

    it 'updates movie' do
      patch :update, params: {id: car.id, car: {name: 'Audi'}} 
      expect(Car.first.name).not_to eq(car.name)
    end
  end

  describe 'DELETE destroy' do
    let!(:car) { create(:car, name: 'Audi') }

    it 'deletes movie' do
      delete :destroy, params: {id: car.id} 
      expect(Car.first).to be_nil
    end
  end
end
