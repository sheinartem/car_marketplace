require 'rails_helper'

RSpec.describe DealersController, type: :controller do
  describe "GET index" do
    let(:dealer1) { create(:dealer, name: 'Ed') }
    let(:dealer2) { create(:dealer, name: 'Joe') } 

    it 'assigns @dealers' do
      get :index
      expect(assigns(:dealers)).to eq([dealer1, dealer2])
    end
    
    it "has a 200 status code" do
      get :index
      expect(response.status).to eq(200)
    end
  end

  describe 'GET show' do
    let(:dealer) { create(:dealer, name: 'Joe') }

    it 'assigns variables' do
      get :show, params: {id: dealer.id}
      expect(assigns(:dealer)).to eq(dealer)
    end

    it "has a 200 status code" do
      get :show, params: {id: dealer.id}
      expect(response.status).to eq(200)
    end
  end

  describe 'GET new' do
    let!(:dealer) { create(:dealer, name: 'Joe') }

    it 'assigns @dealer' do
      get :new
      expect(assigns(:dealer)).to be_a(Dealer)
    end
    
    it "has a 200 status code" do
      get :new
      expect(response.status).to eq(200)
    end
  end

  describe 'POST create' do

    it 'creates dealer' do
      expect(Dealer.first).to be_nil
      post :create, params: {dealer: {name: 'Joe'} } 
      expect(Dealer.where(name: 'Joe').first).not_to be_nil
    end

    it "has a 302 status code" do
      post :create, params: {dealer: {name: 'Joe'} } 
      expect(response.status).to eq(302)
    end
  end

  describe 'PUT update' do  
    let!(:dealer) { create(:dealer, name: 'Ed') }

    it 'updates movie' do
      patch :update, params: {id: dealer.id, dealer: {name: 'Joe'}} 
      expect(Dealer.first.name).not_to eq(dealer.name)
    end
  end

  describe 'DELETE destroy' do
    let!(:dealer) { create(:dealer, name: 'Joe') }

    it 'deletes movie' do
      delete :destroy, params: {id: dealer.id} 
      expect(Dealer.first).to be_nil
    end
  end
end